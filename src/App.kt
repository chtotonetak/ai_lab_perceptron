import javafx.application.Application
import javafx.fxml.FXMLLoader.load
import javafx.scene.Parent
import javafx.scene.Scene
import javafx.stage.Stage

class App : Application() {

    private val layout = "/app.fxml"

    override fun start(primaryStage: Stage?) {
        primaryStage?.title = "Перцептрон"
        primaryStage?.scene = Scene(load<Parent?>(App::class.java.getResource(layout)))
        primaryStage?.isResizable = false
        primaryStage?.show()
    }

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            launch(App::class.java)
        }
    }
}