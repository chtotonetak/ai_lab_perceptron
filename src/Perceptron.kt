import java.util.*

class Perceptron(val trainSet: List<Point>) {

    private var xWeight = Random().nextDouble()
    private var cWeight = Random().nextDouble()
    private val speedFactor = 0.0001
    private val iterationCount = 1000

    init {
        if (trainSet.isEmpty()) {
            println("Train set is empty...");
            System.exit(1)
        } else {
            println("Starting training...")
            for (i in 0 until iterationCount) {
                train()
            }
            println("Line: y = $xWeight * x + ($cWeight)")
        }
    }

    private fun train() {
        for (p in trainSet) {
            val ref = p.x * xWeight + cWeight
            if ( p.isOnPlane && (ref < p.y)
                || !p.isOnPlane && (ref > p.y)) {
                xWeight += (p.y - ref) * p.x * speedFactor
                cWeight += (p.y - ref) * speedFactor
            }
        }
    }

    fun classify(x: Int, y: Int): Boolean {
        return x * xWeight + cWeight > y //
    }

    fun getWeights(): List<Double> {
        return listOf(xWeight, cWeight)
    }
}