import com.csvreader.CsvReader
import javafx.fxml.FXML
import javafx.scene.control.Button
import javafx.scene.layout.VBox
import javafx.stage.FileChooser
import org.jfree.chart.ChartFactory
import org.jfree.chart.JFreeChart
import org.jfree.chart.StandardChartTheme
import org.jfree.chart.block.BlockBorder
import org.jfree.chart.fx.ChartViewer
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer
import org.jfree.data.xy.XYDataset
import org.jfree.data.xy.XYSeries
import org.jfree.data.xy.XYSeriesCollection
import java.awt.Font

class Controller {
    @FXML
    lateinit var mainBtn: Button

    @FXML
    lateinit var centerPane: VBox


    fun initialize() {
        mainBtn.setOnAction { mainBtnClicked() }
    }

    private fun mainBtnClicked() {
        // Read data from CSV and training
        val trainSet = readData()
        val prc = Perceptron(trainSet)
        val lineParams = prc.getWeights()

        // Test set
        println("(0;20) => ${prc.classify(0, 20)}")
        println("(25;-20) => ${prc.classify(25, -20)}")

        // Output points on screen for visual debug
        val dataset = createDataset(trainSet, lineParams)
        val chart = createChart(dataset)
        val viewer = ChartViewer(chart)

        centerPane.children.clear()
        centerPane.children.add(viewer)
    }

    private fun createDataset(points: List<Point>, lineParams: List<Double>): XYDataset {
        val dataset = XYSeriesCollection()

        if (!points.isEmpty()) {
            val trueSet = XYSeries("Входят")
            val falseSet = XYSeries("Не входят")

            var minX = points[0].x
            var maxX = 0
            for (p in points) {
                if (p.isOnPlane) {
                    trueSet.add(p.x, p.y)
                } else {
                    falseSet.add(p.x, p.y)
                }

                if (p.x < minX) minX = p.x
                if (p.x > maxX) maxX = p.x
            }

            val lineSet = XYSeries("Линия раздела")
            if (lineParams.size == 2) {
                lineSet.add(minX.toDouble(), minX * lineParams[0] + lineParams[1])
                lineSet.add(maxX.toDouble(), maxX * lineParams[0] + lineParams[1])
            }

            dataset.addSeries(trueSet)
            dataset.addSeries(falseSet)
            dataset.addSeries(lineSet)
        }

        return dataset
    }

    private fun createChart(dataset: XYDataset): JFreeChart {
        val chart_xy = ChartFactory.createScatterPlot(
                "",
                "X",
                "Y",
                dataset)

        // Font styling
        val fontFamily = "Arial"
        val chartTheme = StandardChartTheme.createJFreeTheme() as StandardChartTheme

        val oldExtraLargeFont = chartTheme.extraLargeFont
        val oldLargeFont = chartTheme.largeFont
        val oldRegularFont = chartTheme.regularFont
        val oldSmallFont = chartTheme.smallFont

        val extraLargeFont = Font(fontFamily, oldExtraLargeFont.style, oldExtraLargeFont.size)
        val largeFont = Font(fontFamily, oldLargeFont.style, oldLargeFont.size)
        val regularFont = Font(fontFamily, oldRegularFont.style, oldRegularFont.size)
        val smallFont = Font(fontFamily, oldSmallFont.style, oldSmallFont.size)

        chartTheme.extraLargeFont = extraLargeFont
        chartTheme.largeFont = largeFont
        chartTheme.regularFont = regularFont
        chartTheme.smallFont = smallFont
        /*
        chartTheme.legendBackgroundPaint = darculaGREY
        chartTheme.legendItemPaint = darculaLIGHT
        chartTheme.chartBackgroundPaint = darculaGREY
        chartTheme.axisLabelPaint = darculaLIGHT
        chartTheme.tickLabelPaint = darculaLIGHT
        */
        chartTheme.apply(chart_xy)
        chart_xy.antiAlias = true
        chart_xy.legend.frame = BlockBorder.NONE

        // Plot styling
        val plot = chart_xy.xyPlot
        val renderer = XYLineAndShapeRenderer()
        // Remove lines visibility
        for (i in 0 until dataset.seriesCount - 1) {
            renderer.setSeriesLinesVisible(i, false)
        }

        // Background
//        plot.backgroundPaint = java.awt.Color(204, 204,204)
        plot.backgroundPaint = java.awt.Color(17, 17,17)
//        plot.rangeGridlinePaint = java.awt.Color.WHITE
//        plot.domainGridlinePaint = java.awt.Color.WHITE

        plot.renderer = renderer

        // Set range
        /*
        val yAxis = plot.rangeAxis
        yAxis.isAutoRange = false
        yAxis.setRange(0.0, 100.0)
        // Set domain
        val xAxis = plot.domainAxis
        xAxis.isAutoRange = false
        xAxis.setRange(0.0, 100.0)
        */

        return chart_xy
    }

    private fun readData(): ArrayList<Point> {
        val output = ArrayList<Point>()
        val fileChooser = FileChooser()
        fileChooser.title = "Open CSV File"
        fileChooser.extensionFilters.addAll(listOf(FileChooser.ExtensionFilter("CSV", "*.csv")))
        val file = fileChooser.showOpenDialog(centerPane.scene.window)

        if (file != null) {
            val reader = CsvReader(file.absolutePath).apply {
                trimWhitespace = true
                skipEmptyRecords = true
                delimiter = ';'
                readHeaders()
            }
            while (reader.readRecord()) {
                //println("(${reader[0]};${reader[1]}) - ${reader[2]}")
                output.add(Point(reader[0].toInt(), reader[1].toInt(), reader[2] == "1"))
            }
        }
        return output
    }
}